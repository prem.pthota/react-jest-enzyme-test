import React from "react";
export default class Arithmetic extends React.Component {
  constructor(props) {
  super(props);
  this.state = {
    counter:0
  }
  }
  render() {
    const {counter}=this.state
    return (
    <div>
     <span id="sum">{this.props.num1+this.props.num2}</span>
     <span id="difference">{this.props.num1-this.props.num2}</span>
     <span id="product">{this.props.num1*this.props.num2}</span>
     <span id="quotient">{this.props.num1%this.props.num2}</span>
     <span id="initial_count_value">{counter}</span>
     <button onClick={()=>this.setState({counter:counter+1})}>count</button>
    </div>
  )
}
}