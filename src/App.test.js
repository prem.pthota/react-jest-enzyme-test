import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App'

Enzyme.configure({ adapter: new Adapter() });


describe('With Enzyme', () => {
  it('Test case for H1 tag', () => {
    const app = shallow(
      <App />
    )
    expect(app.find('h1').text()).toEqual('Hello World')
  })
})

describe('With Enzyme', () => {
  it('Test case for Props', () => {
    const app = shallow(
      <App name='john'/>
    )
    expect(app.find('#welcome').text()).toEqual('welcome john')
  })
})

test('Test case for H2 tag',()=>{
  const app =shallow(<App />)
  expect(app.find('h2').text()).toEqual('How are you')
})

