import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Arithmetic from '../components/Arithmetic'

Enzyme.configure({ adapter: new Adapter() });


describe('With Enzyme Props', () => {
  it('Test cases for Arithmetic Operations', () => {
    const arithmetic = shallow(
      <Arithmetic num1={4} num2={2} />
    )
    expect(arithmetic.find('#sum').text()).toEqual("6")
    expect(arithmetic.find('#difference').text()).toEqual("2")
    expect(arithmetic.find('#product').text()).toEqual("8")
    expect(arithmetic.find('#quotient').text()).toEqual("0")
  })
})


describe('With Enzyme State', () => {
  it('Test cases for Count Operations after increment value', () => {
    const counter = shallow(
      <Arithmetic/>
    )
    expect(counter.find('#initial_count_value').text()).toBe("0")
  })
})


describe('With Enzyme Onclick', () => {
  it('Test cases for Count Operations', () => {
    const counter = shallow(
      <Arithmetic/>
    )
    counter.find('button').simulate('click');
    expect(counter.find('#initial_count_value').text()).toEqual("1");
  })
})